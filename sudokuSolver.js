$(init);

function init(){
  var grid = new SudokuGrid();
  $("button").click(function(){
    grid.parseHTMLTable("sudoku-grid");
    solve(grid);
  });

  $("p").each(function(){
    console.log($(this).attr("class"));
    $(this).text(($(this).attr("class")).charAt(1));
  });
}

/* solve(grid) function
 * Solve a SUDOKU grid
 */
function solve(grid){
  //loop through grid until complete
  var countLoop = 0;
  while (!grid.isComplete() && countLoop < 1000){
    //update the possible values in the grid
    grid.updatePossibilities();

    //find all entries that have a unique value
    var uniquePossibilities = grid.findUniquePossibilities();

    //update the values of all entries that have a unique value
    for(var i = 0; i < uniquePossibilities.length; i++){
      var square = uniquePossibilities[i][0];
      var entry = uniquePossibilities[i][1];
      var value = uniquePossibilities[i][2]
      grid.changeEntry(square, entry, value);
    }
    countLoop++;
  }

  //WE DONE BOI!
  console.log("COMPLETED");
  grid.loadGrid("sudoku-grid");
}

/* SudokuGrid object
 * represents a grid of sudoku
 */
function SudokuGrid() {
  /* isComplete() function
   * returns true if the grid is COMPLETED
   * false otherwise
   */
  this.isComplete = function(){
    //loop through all entries
    for(var i = 0; i < this.grid.length; i++){
      for(var j = 0; j < this.grid[i].length; j++){
        //if an entry does not have a number then returns false (aka. has -1)
        if(this.grid[i][j] == -1){
          return false;
        }
      }
    }
    return true;
  }

  /* updatePossibilities() function
   * updates the possibilitiesGird
   */
  this.updatePossibilities = function(){
    //all the entries with values don't have any possibilties
    this.updateEntriesThatContainValues();

    //for each value in a square no entry in that square should have that
    //value as possibilty
    this.updateSquaresAccordingToExistingValues();

    //no row has an entry with the possibilty of one of its already
    ///existing values
    this.updatePossibilitiesOfRows();

    //no column has an entry with the possibilty of one of its already
    //exisitng values
    this.updatePossibilitiesOfColumns();

    //if there are only 2 or 3 possibilities in 1 square for the same value
    //and these are in a straight line
    //then the rest of the row or column on which they are in line cannot
    //contain that value
    this.updateAlignedPossibilities();

    //if an entry is the only one to have a value as a possibilty then its
    //value is that possibilty
    //loop through each square
    this.updateUniquePossibilities();
  }

  this.updateAlignedPossibilitiesRow = function(){
    //holds all possible values for an entry
    var values = [1,2,3,4,5,6,7,8,9];
    //loop through each value
    for(var valueIndex = 0; valueIndex < values.length; valueIndex++){
      //for the given row count how many possibilities exist for the given value
      for(var row = 0; row < 9; row++){
        //holds the number of possibilities in given row for given value for each square
        var possibilitiesPerSquare = [0, 0, 0];

        //for each square count how many possibilities lie in it (in the row)
        var firstSquare = Math.floor(row/3) * 3;
        var firstEntry = (row % 3) * 3;
        for(var square = firstSquare; square < firstSquare + 3; square++){
          for(var entry = firstEntry; entry < firstEntry + 3; entry++){
            if(this.possibilitiesGrid[square][entry][values[valueIndex] - 1]){
              possibilitiesPerSquare[square%3]++;
            }
          }
        }

        var indexOfSquareThatHasSwag = -1;
        //check for each square that only the possibilities which are aligned are the ones in the square
        for(var squareIndex = 0; squareIndex < possibilitiesPerSquare.length; squareIndex++){
          if(possibilitiesPerSquare[squareIndex] > 1){
            var possibilityInSquare = 0;
            for(var entry = 0; entry < this.possibilitiesGrid[firstSquare + squareIndex].length; entry++){
              if(this.possibilitiesGrid[firstSquare + squareIndex][entry][values[valueIndex] - 1]){
                possibilityInSquare++;
              }
            }
            if(possibilityInSquare == possibilitiesPerSquare[squareIndex]){
              //then the square has alligned possibilites
              indexOfSquareThatHasSwag = firstSquare + squareIndex;
            }
          }
        }

        if(indexOfSquareThatHasSwag != -1){
          for(var square = firstSquare; square < firstSquare + 3; square++){
            if(square != indexOfSquareThatHasSwag){
              for(var entry = firstEntry; entry < firstEntry + 3; entry++){
                this.possibilitiesGrid[square][entry][values[valueIndex] - 1] = false;
              }
            }
          }
        }
      }
    }
  }

  this.updateAlignedPossibilitiesColumn = function(){
    //holds all possible values for an entry
    var values = [1,2,3,4,5,6,7,8,9];
    //loop through each value
    for(var valueIndex = 0; valueIndex < values.length; valueIndex++){
      for(var column = 0; column < 9; column++){
        //holds the number of possibilities in given row for given value for each square
        var possibilitiesPerSquare = [0, 0, 0];

        //for each square count how many possibilities lie in it (in the row)
        var firstSquare = Math.floor(column / 3);
        var firstEntry = column % 3;
        for(var square = firstSquare; square <= firstSquare + 6; square += 3){
          for(var entry = firstEntry; entry <= firstEntry + 6; entry += 3){
            if(this.possibilitiesGrid[square][entry][values[valueIndex] - 1]){
              possibilitiesPerSquare[Math.floor(square/3)]++;
            }
          }
        }

        var indexOfSquareThatHasSwag = -1;
        //check for each square that only the possibilities which are aligned are the ones in the square
        for(var squareIndex = 0; squareIndex < possibilitiesPerSquare.length; squareIndex++){
          if(possibilitiesPerSquare[squareIndex] > 1){
            var possibilityInSquare = 0;
            for(var entry = 0; entry < this.possibilitiesGrid[firstSquare + 3 * squareIndex].length; entry++){
              if(this.possibilitiesGrid[firstSquare + 3 * squareIndex][entry][values[valueIndex] - 1]){
                possibilityInSquare++;
              }
            }
            if(possibilityInSquare == possibilitiesPerSquare[squareIndex]){
              //then the square has alligned possibilites
              indexOfSquareThatHasSwag = firstSquare + 3*squareIndex;
            }
          }
        }

        if(indexOfSquareThatHasSwag != -1){
          for(var square = firstSquare; square <= firstSquare + 6; square += 3){
            if(square != indexOfSquareThatHasSwag){
              for(var entry = firstEntry; entry <= firstEntry + 6; entry += 3){
                this.possibilitiesGrid[square][entry][values[valueIndex] - 1] = false;
              }
            }
          }
        }
      }
    }
  }

  this.updateAlignedPossibilities = function(){
    this.updateAlignedPossibilitiesRow();
    this.updateAlignedPossibilitiesColumn();
  }

  this.updateUniquePossibilities = function(){
    for(var i = 0; i < this.possibilitiesGrid.length; i++){
      //holds the number of possibilties for each value in given square
      var countForEachValue = [0, 0, 0, 0, 0, 0, 0, 0, 0];
      //loop through each entry of square
      for(var j = 0; j < this.possibilitiesGrid[i].length; j++){
        //loop through each entry
        for(var k = 0; k < this.possibilitiesGrid[i][j].length; k++){
          //add 1 to each value that is possible for that entry
          if(this.possibilitiesGrid[i][j][k]){
            countForEachValue[k]++;
          }
        }
      }

      // find the indexes for the given square that
      var indexesOfUniqueValues = []
      //loop through the counts for each value
      for(var valueIndex = 0; valueIndex < countForEachValue.length; valueIndex++){
        //if the count is 1 then add this indes to indexesOfUniqueValues
        if(countForEachValue[valueIndex] == 1){
          indexesOfUniqueValues.push(valueIndex);
        }
      }

      //loop through every entry
      for(var j = 0; j < this.possibilitiesGrid[i].length; j++){
        //loop through every possibility
        for(var k = 0; k < this.possibilitiesGrid[i][j].length; k++){
          //if k is an index with a unique value && k is true for this index
          if(indexesOfUniqueValues.includes(k) && this.possibilitiesGrid[i][j][k]){
            //turn all the other possibilities to false
            for(var possIndex = 0; possIndex < this.possibilitiesGrid[i][j].length; possIndex++){
              if(possIndex != k){
                this.possibilitiesGrid[i][j][possIndex] = false;
              }
            }
          }
        }
      }
    }
  }

  this.updatePossibilitiesOfRows = function(){
    for(var i = 0; i < 9; i++){
      var values = this.getValuesInRow(i);
      var firstSquare = Math.floor(i/3) * 3;
      var firstEntry = 0;
      for(var square = firstSquare; square < firstSquare + 3; square++){
        firstEntry = (i % 3) * 3;
        for(var entry = firstEntry; entry < firstEntry + 3; entry++){
          for(var k = 0; k < this.possibilitiesGrid[square][entry].length; k++){
            if(values.includes(k+1) && this.possibilitiesGrid[square][entry][k]){
              this.possibilitiesGrid[square][entry][k] = false;
            }
          }
        }
      }
    }
  }

  this.updatePossibilitiesOfColumns = function(){
    for(var i = 0; i < 9; i++){
      var values = this.getValuesInColumn(i);
      var firstSquare = Math.floor(i / 3);
      var firstEntry = 0;
      for(var square = firstSquare; square <= firstSquare + 6; square += 3){
        firstEntry = i % 3;
        for(var entry = firstEntry; entry <= firstEntry + 6; entry += 3){
          for(var k = 0; k < this.possibilitiesGrid[square][entry].length; k++){
            if(values.includes(k+1) && this.possibilitiesGrid[square][entry][k]){
              this.possibilitiesGrid[square][entry][k] = false;
            }
          }
        }
      }
    }
  }

  this.updateSquaresAccordingToExistingValues = function(){
    //loop through every square
    for(var i = 0; i < this.possibilitiesGrid.length; i++){
      //for each square get the values it contains
      var values = this.getValuesInSquare(i);
      //loop through each entry
      for(var j = 0; j < this.possibilitiesGrid[i].length; j++){
        //loop through each possibilty
        for(var k = 0; k < this.possibilitiesGrid[i][j].length; k++){
          //for each value make the possibility of that value false
          if(values.includes(k+1) && this.possibilitiesGrid[i][j][k]){
            this.possibilitiesGrid[i][j][k] = false;
          }
        }
      }
    }
  }

  this.updateEntriesThatContainValues = function(){
    //loop through every square
    for(var i = 0; i < this.grid.length; i++){
      //loop through every entry
      for(var j = 0; j < this.grid[i].length; j++){
        //if the given entry does not have a value
        if (this.grid[i][j] != -1){
          //loop through all possibilties and set them to false
          for(var k = 0; k < this.possibilitiesGrid[i][j].length; k++){
            this.possibilitiesGrid[i][j][k] = false;
          }
        }
      }
    }
  }

  this.getValuesInRow = function(rowIndex){
    var values = [];
    var firstSquare = Math.floor(rowIndex/3) * 3;
    var firstEntry = 0;
    for(var square = firstSquare; square < firstSquare + 3; square++){
      firstEntry = (rowIndex % 3) * 3;
      for(var entry = firstEntry; entry < firstEntry + 3; entry++){
        if(this.grid[square][entry] != -1){
          values.push(this.grid[square][entry]);
        }
      }
    }
    return values;
  }

  this.getValuesInColumn = function(columnIndex){
    var values = [];
    var firstSquare = Math.floor(columnIndex / 3);
    var firstEntry = 0;
    for(var square = firstSquare; square <= firstSquare + 6; square += 3){
      firstEntry = columnIndex % 3;
      for(var entry = firstEntry; entry <= firstEntry + 6; entry += 3){
        if(this.grid[square][entry] != -1){
          values.push(this.grid[square][entry]);
        }
      }
    }
    return values;
  }

  /* getValuesInSquare(squareIndex) function
   * returns all the the values for the entries of the given square
   */
  this.getValuesInSquare = function(squareIndex){
    //holds all the value that the square contains
    var values = [];

    //loop through the every entry in the given square
    for(var j = 0; j < this.grid[squareIndex].length; j++){
      var value = this.grid[squareIndex][j];

      //if the value is not -1 (aka is an actual value)
      //the value is added to allValues
      if(value != -1){
        values.push(value);
      }
    }
    return values;
  }

  /* findUniquePossibilities() function
   * returns an array containing every entry that has only one possible value
   * the entry is represented by [square, entryIndex, uniqueValue]
   */
  this.findUniquePossibilities = function(){
    //array to hold all unique possibilties
    var uniquePossibilities = [];

    //loop through every square in the possibilities grid
    for(var i = 0; i < this.possibilitiesGrid.length; i++){

      //loop through every entry of each square
      for(var j = 0; j < this.possibilitiesGrid[i].length; j++){

        //track many possibilties there are
        var countNumPossibilities = 0;
        var value = 0;
        //loop through every possibilty
        for(var k = 0; k < this.possibilitiesGrid[i][j].length; k++){

          //if(there is a possibilty) add 1 to the count
          if(this.possibilitiesGrid[i][j][k]){
            countNumPossibilities++;
            value = k + 1;
          }
        }

        //if there is only one possibilty
        if(countNumPossibilities == 1){
          //adds the square, the entry, and the value (value is the index + 1)
          //since values start at 1;
          uniquePossibilities.push([i, j, value]);
        }
      }
    }

    //returns every unique possibilty
    return uniquePossibilities;
  }


  /* changeEntry(squareIndex, entryIndex, value) function
   * Change the value of the given entry at the given square with given value
   */
  this.changeEntry = function(squareIndex, entryIndex, value){
    this.grid[squareIndex][entryIndex] = value;
  }

  //grid of all the values in the grid
  this.grid = [
    [-1, -1, -1, -1, -1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1, -1, -1, -1, -1]
  ];

  //grid of all the possibilities for each entry
  this.possibilitiesGrid = [
    [
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true]
    ],

    [
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true]
    ],

    [
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true]
    ],

    [
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true]
    ],

    [
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true]
    ],

    [
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true]
    ],

    [
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true]
    ],

    [
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true]
    ],

    [
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true]
    ],
  ];

  this.parseHTMLTable = function(tableId){
    var i = 0;
    var j = 0;
    var values = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    gridRef = this.grid;
    $("#" + tableId).find("table").each(function(){
      $square = $(this);
      j = 0;
      $square.find("input").each(function(){
        var value = Number($(this).val().trim());
        if(values.includes(value)){
          gridRef[i][j] = value;
        }
        j++;
      });
      i++;
    });
  }

  this.loadGrid = function(tableId){
    var i = 0;
    var j = 0;
    gridRef = this.grid;
    posRef = this.possibilitiesGrid;
    $("#" + tableId).find("table").each(function(){
      $square = $(this);
      j = 0;
      $square.find("input").each(function(){
        if(gridRef[i][j] != -1){
          $(this).val(gridRef[i][j]);
        } else {
          $(this).val("");
        }
        for(var k = 0; k < posRef[i][j].length; k++){
          if(posRef[i][j][k]){
            $(this).parent().find("._" + (k+1)).css("display", "block");
          }
        }
        j++;
      });
      i++;
    });
  }
}
