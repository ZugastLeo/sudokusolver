function SudokuGrid() {

  /****************************************************************************/
  /****************************************************************************/
  /*******************************NUMBERS**************************************/
  /****************************************************************************/
  /****************************************************************************/
  this.getSquare = function(index) {
    return this.grid[index];
  };

  this.getColumn = function(index) {
    var column = [];
  };

  this.getRow = function(index) {

  };

  this.getEntryInSquare = function(squareIndex, row, col) {

  };

  this.getEntryInGrid = function(row, col) {

  };

  this.grid = [
    [-1, -1, -1, -1, -1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1, -1, -1, -1, -1]
  ];

  /****************************************************************************/
  /****************************************************************************/
  /*******************************POSSIBILITIES********************************/
  /****************************************************************************/
  /****************************************************************************/

  this.canEntryContainNumber = function(row, col, num) {

  };

  this.getPossilitySquare = function(index) {
    return this.possibilitiesGird[index];
  };

  this.getPossilityColumn = function(index) {

  };

  this.getPossilityRow = function(index) {

  };

  this.getEntryInPossilitySquare = function(squareIndex, row, col) {

  };

  this.getEntryInPossilityGrid = function(row, col) {

  };

  this.possibilitiesGird = [
    [
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true]
    ],

    [
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true]
    ],

    [
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true]
    ],

    [
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true]
    ],

    [
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true]
    ],

    [
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true]
    ],

    [
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true]
    ],

    [
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true]
    ],

    [
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true]
    ],

    [
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true],
      [true, true, true, true, true, true, true, true, true]
    ]
  ];
}
